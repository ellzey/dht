#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "tree.h"

enum {
    DHT_SERV_STATUS_DOWN,
    DHT_SERV_STATUS_UP
};

struct dht_server {
    RB_ENTRY(dht_server) dht_server_n;
    char           *host;
    uint16_t        port;
    uint64_t        hash;
    void           *usrdata;
    int             status;

    /*
     * used at the very first node and 
     * has a list of replicas for fast 
     * modification 
     */
    struct dht_server *sub_replicas;

    /*
     * we use the first_server for sub replicas 
     * to point back to the first node 
     */
    struct dht_server *first_server;
};

struct dht {
    uint16_t        replicas;
    struct dht_tree *dht_tree;
    void           *usrdata;
};

int             dht_hash_cmp(const struct dht_server *a,
                             const struct dht_server *b);
uint64_t        dht_sha1_hash(char *);
int             dht_set_server_status(struct dht *, struct dht_server *,
                                      int);
int             dht_del_server(struct dht *dht, struct dht_server *server);
struct dht_server *dht_add_server(struct dht *, const char *,
                                  const uint16_t, void *);
struct dht_server *dht_fetch_server(struct dht *dht, char *key);
struct dht     *dht_init(int);
