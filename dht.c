#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "tree.h"
#include "dht.h"
#include "sha1.h"

RB_HEAD(dht_tree, dht_server) dht_tree_head =
RB_INITIALIZER(&___dht_tree_head___);
RB_PROTOTYPE(dht_tree, dht_server, dht_server_n, dht_hash_cmp);
RB_GENERATE(dht_tree, dht_server, dht_server_n, dht_hash_cmp);

     
int dht_hash_cmp(const struct dht_server *a,
                 const struct dht_server *b)
{
    if (a->hash < b->hash)
        return -1;

    if (a->hash > b->hash)
        return 1;

    return 0;
}

uint64_t
dht_sha1_hash(char *data)
{
    SHA1_CTX       *ctx;
    unsigned char  *output;
    uint64_t        key;
    uint32_t        upper,
                    lower;

    if (!(output = malloc(20)))
        return 0;

    ctx = malloc(sizeof(SHA1_CTX));
    key = 0;

    SHA1Init(ctx);
    SHA1Update(ctx, (unsigned char *) data, strlen(data));
    SHA1Final(output, ctx);


    upper = (uint32_t) ((output[7] << 24) |
                        (output[6] << 16) |
                        (output[5] << 8) | (output[4]));

    lower = (uint32_t) ((output[3] << 24) |
                        (output[2] << 16) |
                        (output[1] << 8) | (output[0]));

    key = ((uint64_t) upper << 32) | (uint64_t) lower;

    free(ctx);
    free(output);
    return key;
}

int
dht_set_server_status(struct dht *dht, struct dht_server *serv, int status)
{
    /*
     * iterate through all of the nodes in a dht_server and
     * status to whatever 
     */

    struct dht_server *found;

    found = NULL;

    if (!dht || !serv)
        return -1;

    found = serv->first_server;

    if (!found)
        return -1;

    while (found != NULL) {
        found->status = status;
        found = found->sub_replicas;
    }

    return 0;
}

int
dht_del_server(struct dht *dht, struct dht_server *server)
{
    /*
     * from the first node, iter through all of the replicas
     * delete from the rb tree, then free the resources 
     */
    struct dht_server *found;

    found = NULL;

    if (!dht || !server)
        return -1;

    found = server->first_server;

    if (!found)
        return -1;

    while (found != NULL) {
        struct dht_server *serv;

        serv = found;
        RB_REMOVE(dht_tree, dht->dht_tree, serv);
        found = serv->sub_replicas;

        free(serv->host);
        free(serv);
    }

    return 0;
}

struct dht_server *
dht_add_server(struct dht *dht,
               const char *host, const uint16_t port, void *usrdata)
{
    /*
     * create all iterations (replicas) of a dht_server struct and
     * insert it into the rb tree using the sha1 64bit key for insertion 
     */

    struct dht_server *first;
    int             i,
                    hash_sz;
    char           *hash_str;

    first = NULL;

    if (!host || !port || !dht)
        return NULL;

    hash_sz = strlen(host) + 5 +        /* max strlen of 65535 (port) */
        5 +                     /* max strlen of 65535 (replica #) */
        1;                      /* trailing \0 */

    if (!(hash_str = calloc(hash_sz, 1)))
        return NULL;

    for (i = 0; i < dht->replicas; i++) {
        struct dht_server *serv;

        snprintf(hash_str, hash_sz - 1, "%s%d%d", host, port, i);

        if (!(serv = calloc(sizeof(struct dht_server), 1))) {
            free(hash_str);
            return NULL;
        }

        if (!(serv->hash = dht_sha1_hash(hash_str))) {
            free(hash_str);
            free(serv);
            return NULL;
        }


        /*
         * set the initial status as down 
         */
        serv->status = DHT_SERV_STATUS_DOWN;
        serv->usrdata = usrdata;
        serv->host = strdup(host);
        serv->port = port;

        if (!i) {
            serv->sub_replicas = NULL;
            serv->first_server = serv;
            first = serv;
        } else {
            serv->first_server = first;
            serv->sub_replicas = first->sub_replicas;
            first->sub_replicas = serv;
        }

        RB_INSERT(dht_tree, dht->dht_tree, serv);
    }

    free(hash_str);
    return first;
}

struct dht_server *
dht_fetch_server(struct dht *dht, char *key)
{
    struct dht_server *node;
    struct dht_server *found;

    if (!(node = calloc(sizeof(struct dht_server), 1)))
        return NULL;

    node->hash = dht_sha1_hash(key);

    /*
     * find the next larger key in the rbtree, if it is 
     * not found, we circle around back to the minimum
     * key 
     */

    found = RB_NFIND(dht_tree, dht->dht_tree, node);

    if (!found)
        found = RB_MIN(dht_tree, dht->dht_tree);

    free(node);

    return found;
}

struct dht     *
dht_init(int replicas)
{
    struct dht     *dht;

    if (!(dht = malloc(sizeof(struct dht))))
        return NULL;

    if (!(dht->dht_tree = calloc(sizeof(struct dht_tree *), 1))) {
        free(dht);
        return NULL;
    }

    dht->replicas = replicas;

    return dht;
}
