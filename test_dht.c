#include "dht.h"

int
main(int argc, char **argv)
{
    struct dht     *dht;
    struct dht_server *serv;
    struct dht_server *node_a,
                   *node_b,
                   *node_c;

    dht = dht_init(250);

    node_a = dht_add_server(dht, "192.168.0.1", 5555, NULL);
    node_b = dht_add_server(dht, "192.168.0.2", 5555, NULL);
    node_c = dht_add_server(dht, "192.168.0.3", 5555, NULL);

    dht_set_server_status(dht, node_a, DHT_SERV_STATUS_UP);
    dht_set_server_status(dht, node_b, DHT_SERV_STATUS_UP);
    dht_set_server_status(dht, node_c, DHT_SERV_STATUS_UP);

    serv = dht_fetch_server(dht, argv[1]);

    printf("%s:%d\n", serv->host, serv->port);
    dht_del_server(dht, node_a);
    dht_del_server(dht, node_b);
    dht_del_server(dht, node_c);

    serv = dht_fetch_server(dht, argv[1]);
    printf("%p\n", serv);

    free(dht->dht_tree);
    free(dht);
    return 0;
}
